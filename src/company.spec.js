const company = require('./company');

//use ids 3, 85
describe('company', () => {
    it ('company=3', () => {
        expect(company(3)).toMatchSnapshot();
    });
    it ('company=85', () => {
        expect(company(85)).toMatchSnapshot();
    });
    it ('company=microsoft', () => {
        expect(() => {
            company('microsoft');
        }).toThrowError();
    });
});