const lowerCase = require('./lowercase');

describe('lower-case', () => {
    it('CAT => cat', () => {
        expect(lowerCase('CAT')).toBe('cat');
    });
    it('error - bad input', () => {
        expect(() => {
            lowerCase({ 'word': 'cat' });
        }).toThrow(/bad input/);
    });
});
