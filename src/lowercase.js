var lowerCase = require('lower-case');

lowerCase(null);           //=> ""
lowerCase('STRING');       //=> "string"
lowerCase('STRING', 'tr'); //=> "strıng"
 
lowerCase({ toString: function () { return 'TEST'; } }); //=> "test"

module.exports = function lowerCase2(name) {
    if (typeof name !== 'string') {
        throw new Error('bad input');
    }
    return lowerCase(name);
};